/* DEPRECATED: rExMMPMP
 */
class rExMMPMP {
  /** Constructor. Expects the contents of a file coming in.
   */
  constructor(_file) {
    this._filelines = rExMMPMP._linefy(_file)
  }
  /** Return _filelines as a savable string version.
   */
  get FILEBACK() {
    return this._filelines.join("\n")
  }
  /** Return filelines cleaned for assertions.
   */
  get FILELINES() {
    return this._filelines
  }
  /** Return filelines cleaned for assertions.
   */
  get TESTSTATE() {
    return this._filelines.filter((word) => word.length > 0).join("")
  }

  /** Change matching filelines
   * @param {STRING} _matchWith Look for a line of text matching this
   * @param {STRING} _overwriteWith Replace the whole line with this
   *
   * If the item hasn't matched, it will get added between After and Until or the end of the settings _list.
   */
  findOverwrite(_matchWith, _overwriteWith) {
    return this.findOverwriteAfterUntil(
      _matchWith,
      _overwriteWith,
      "",
      "ThisWillNeverGetMatched",
    )
  }

  /** Change matching filelines, depending on coming between a the line order.
   * @param {STRING} _matchWith Look for a line of text matching this
   * @param {STRING} _overwriteWith Replace the whole line with this
   * @param {STRING} _afterFinding Replace only after this line of text has been matched
   * @param {STRING} _untilFinding Replace until this line of text has been matched
   *
   * If the item hasn't matched, it will get added between After and Until or the end of the settings _list.
   */
  findOverwriteAfterUntil(
    _matchWith,
    _overwriteWith,
    _afterFinding,
    _untilFinding,
  ) {
    this._filelines = rExMMPMP._soLineByLine(
      _matchWith,
      _overwriteWith,
      this._filelines,
      _afterFinding,
      _untilFinding,
    )
    return this._filelines
  }

  /** Change a line the settings file based on some match
   * @param {STRING} _matchWith Look for a line of text matching this.
   * @param {STRING} _overwriteWith Replace the whole line with this.
   * @param {STRING} _list A file as a list of lines.
   * @param {STRING} _afterFinding Replace only after this line of text has been matched.
   * @param {STRING} _untilFinding Replace until this line of text has been matched.
   *
   * If the item hasn't matched, it will get added between After and Until or the end of the settings _list.
   */
  static _soLineByLine(
    _matchWith,
    _overwriteWith,
    _list,
    _afterFinding,
    _untilFinding,
  ) {
    if (!_afterFinding || _afterFinding === "") _afterFinding = "" // default: start anywhere
    if (!_untilFinding || _untilFinding === "") _untilFinding = "_untilFinding" // default: stop nowhere

    let ISMATCHEDANDREPLACED = false
    let OVERWRITINGON = false
    let iDeal = 0

    for (let i in _list) {
      let LINE = _list[i]
      let COMMADEERED = this._supersplit(LINE, ",").length > 1

      // OVERWRITINGON and _untilFinding match found
      if (OVERWRITINGON && LINE.indexOf(_untilFinding) > -1) {
        // just flag where we found it
        iDeal = i
      }

      // Stop identifying if _untilFinding match found
      if (LINE.indexOf(_untilFinding) > -1) OVERWRITINGON = false

      // Start identifying if _afterFinding match found
      if (iDeal === 0 && LINE.indexOf(_afterFinding) > -1) OVERWRITINGON = true

      // Catching a line which matches with `_matchWith`.
      if (LINE.indexOf(_matchWith) > -1) {
        // Are we _afterFinding match and allowed to overwrite.
        if (OVERWRITINGON) {
          // With lots of commas? Already in the right place? Ignore.
          if (!COMMADEERED) {
            _list[i] = _overwriteWith
            ISMATCHEDANDREPLACED = true
          }
        } else {
          // !OVERWRITINGON
          if (COMMADEERED) {
            // Remove comma separated _matchWith from the comma separated LINE
            _list[i] = LINE.replace(_overwriteWith, "")
          } else {
            _list[i] = ""
          } // End if not OVERWRITINGON but item matched.
        } // End if else for OVERWRITINGON
      } // End if we find a line which matches
    } // End loop

    if (!ISMATCHEDANDREPLACED) {
      if (iDeal > 0) _list.splice(iDeal, 0, _overwriteWith)
      else _list.push(_overwriteWith)
    }

    return _list
  }

  /** Util static method to split a string by linebreaks.
   * @param {STRING} _file_in The contents of a file coming in.
   */
  static _linefy(_file_in) {
    return _file_in.split("\n")
  }

  /** Split and remove empty
   * @param {STRING} _str The contents to split.
   */
  static _supersplit(_str, _sep) {
    return _str
      .trim()
      .split(_sep)
      .filter((word) => word.length > 0)
  }
}

module.exports = rExMMPMP
