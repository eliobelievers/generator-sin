"use strict"
// const path = require("path");
const assert = require("yeoman-assert")
const rExMMPMP = require("../regexpressionsmakemepeemypants.js")

describe("regexpressionsmakemepeemypants.findOverwrite", () => {
  var fileContents = `part 1/a
part 2/b
part 3/a`

  it("confirms the .FILELINES matches the lines we provided", (done) => {
    let rEx = new rExMMPMP(fileContents)
    assert.deepEqual(rEx.FILELINES, ["part 1/a", "part 2/b", "part 3/a"])
    done()
  })

  it("confirms the .FILEBACK property roundtrips fileContents with integrity", (done) => {
    let rEx = new rExMMPMP(fileContents)
    assert.deepEqual(rEx.FILEBACK, fileContents)
    done()
  })

  it("demonstrates how regexpressionsmakemepeemypants works", (done) => {
    let rEx = new rExMMPMP(fileContents)
    // It breaks the files into a list of lines
    assert.deepEqual(rEx.FILELINES, ["part 1/a", "part 2/b", "part 3/a"])
    // it looks for lines matching `someIdentifier` ...
    // ... if it **doesn't** find such a line, it adds someReplacement as a line.
    rEx.findOverwrite("someIdentifier/", "someIdentifier/someReplacement")
    assert.deepEqual(rEx.FILELINES, [
      "part 1/a",
      "part 2/b",
      "part 3/a",
      "someIdentifier/someReplacement",
    ])
    // ... if it **does** find such a line, it swaps it with someReplacement instead.
    rEx.findOverwrite("someIdentifier/", "someIdentifier/forThisReplacement")
    assert.deepEqual(rEx.FILELINES, [
      "part 1/a",
      "part 2/b",
      "part 3/a",
      "someIdentifier/forThisReplacement",
    ])
    rEx.findOverwrite("someIdentifier/", "someIdentifier/forThatReplacement")
    assert.deepEqual(rEx.FILELINES, [
      "part 1/a",
      "part 2/b",
      "part 3/a",
      "someIdentifier/forThatReplacement",
    ])
    rEx.findOverwrite("someIdentifier/", "someIdentifier/forThisReplacement")
    assert.deepEqual(rEx.FILELINES, [
      "part 1/a",
      "part 2/b",
      "part 3/a",
      "someIdentifier/forThisReplacement",
    ])
    rEx.findOverwrite("someIdentifier/", "someIdentifier/forThatReplacement")
    assert.deepEqual(rEx.FILELINES, [
      "part 1/a",
      "part 2/b",
      "part 3/a",
      "someIdentifier/forThatReplacement",
    ])
    rEx.findOverwrite("someIdentifier/", "someIdentifier/toShakeItAllAbout")
    assert.deepEqual(rEx.FILELINES, [
      "part 1/a",
      "part 2/b",
      "part 3/a",
      "someIdentifier/toShakeItAllAbout",
    ])
    // ... if it **does** find such a line, it swaps it with someReplacement.
    // So  **eve**  assumes there should be one of each kind.
    done()
  })
  it("alters existing lines according to a match", (done) => {
    let rEx = new rExMMPMP(fileContents)
    assert.deepEqual(rEx.FILELINES, ["part 1/a", "part 2/b", "part 3/a"])
    rEx.findOverwrite("1/", "part 1/c")
    assert.deepEqual(rEx.FILELINES, ["part 1/c", "part 2/b", "part 3/a"])
    done()
  })
  it("adds lines if there is no match", (done) => {
    let rEx = new rExMMPMP(fileContents)
    assert.deepEqual(rEx.FILELINES, ["part 1/a", "part 2/b", "part 3/a"])
    rEx.findOverwrite("4/", "part 4/c")
    assert.deepEqual(rEx.FILELINES, [
      "part 1/a",
      "part 2/b",
      "part 3/a",
      "part 4/c",
    ])
    done()
  })
  it("forces new on an existing match with this technique", (done) => {
    let rEx = new rExMMPMP(fileContents)
    assert.deepEqual(rEx.FILELINES, ["part 1/a", "part 2/b", "part 3/a"])
    rEx.findOverwrite("someIdentifierGarbage=>ForcesAdd", "part 3/a")
    assert.deepEqual(rEx.FILELINES, [
      "part 1/a",
      "part 2/b",
      "part 3/a",
      "part 3/a",
    ])
    done()
  })
})

describe("regexpressionsmakemepeemypants.findOverwriteAfterUntil", () => {
  var fileContents = `
lonely 1/a

$part1 {
part 1/a
part 2/a
}

$part2 {
part 1/a
part 2/a
}

lonely 1/a
`

  it("confirms the .FILELINES matches the lines we provided", (done) => {
    let rEx = new rExMMPMP(fileContents)
    let fileLinesGoal = [
      "",
      "lonely 1/a",
      "",
      "$part1 {",
      "part 1/a",
      "part 2/a",
      "}",
      "",
      "$part2 {",
      "part 1/a",
      "part 2/a",
      "}",
      "",
      "lonely 1/a",
      "",
    ]
    assert.deepStrictEqual(rEx.FILELINES, fileLinesGoal)
    done()
  })

  it("starts replacing after and until it then stops, early on the page", (done) => {
    var fileGoal = `


$part1 {
part 1/z
part 2/a
}

$part2 {

part 2/a
}


`
    var goal = new rExMMPMP(fileGoal)
    let rEx = new rExMMPMP(fileContents)
    rEx.findOverwriteAfterUntil("1/", "part 1/z", "$part1 {", "}")
    assert.deepStrictEqual(rEx.FILELINES, goal.FILELINES)
    done()
  })

  it("starts replacing after and until it then stops, late on the page", (done) => {
    var fileGoal = `
lonely 1/a

$part1 {
part 1/a
part 2/a
}

$part2 {
part 1/z
part 2/a
}

lonely 1/a
`
    var goal = new rExMMPMP(fileContents)
    let rEx = new rExMMPMP(fileContents)
    // rEx.findOverwriteAfterUntil("1/", "part 1/z", "$part2 {", "}");
    assert.deepStrictEqual(rEx.FILELINES, goal.FILELINES)
    done()
  })

  it("replaces everything because no after or until", (done) => {
    var fileGoal = `
part 1/z

$part1 {
part 1/z
part 2/a
}

$part2 {
part 1/z
part 2/a
}

part 1/z
`
    var goal = new rExMMPMP(fileGoal)
    let rEx = new rExMMPMP(fileContents)
    rEx.findOverwriteAfterUntil("1/", "part 1/z", "", "")
    assert.deepStrictEqual(rEx.FILELINES, goal.FILELINES)
    done()
  })

  it("replaces everything up until, because no after", (done) => {
    var fileGoal = `
part 1/z

$part1 {
part 1/z
part 2/a
}

$part2 {

part 2/a
}


`
    var goal = new rExMMPMP(fileGoal)
    let rEx = new rExMMPMP(fileContents)
    rEx.findOverwriteAfterUntil("1/", "part 1/z", "", "}")
    assert.deepStrictEqual(rEx.FILELINES, goal.FILELINES)
    done()
  })

  it("adds between after and until, early in the page, if no matching line", (done) => {
    var fileGoal = `
lonely 1/a

$part1 {
part 1/a
part 2/a
part 3/z
}

$part2 {
part 1/a
part 2/a
}

lonely 1/a
`
    var goal = new rExMMPMP(fileGoal)
    let rEx = new rExMMPMP(fileContents)
    rEx.findOverwriteAfterUntil("3/", "part 3/z", "$part1 {", "}")
    assert.deepStrictEqual(rEx.FILELINES, goal.FILELINES)
    done()
  })

  it("adds between after and until, late in the page, if no matching line", (done) => {
    var fileGoal = `
lonely 1/a

$part1 {
part 1/a
part 2/a
}

$part2 {
part 1/a
part 2/a
part 3/z
}

lonely 1/a
`
    var goal = new rExMMPMP(fileGoal)
    let rEx = new rExMMPMP(fileContents)
    rEx.findOverwriteAfterUntil("3/", "part 3/z", "$part2 {", "}")
    assert.deepStrictEqual(rEx.FILELINES, goal.FILELINES)
    done()
  })

  it("adds new to end of page with no matching line, after or until", (done) => {
    var fileGoal = `
lonely 1/a

$part1 {
part 1/a
part 2/a
}

$part2 {
part 1/a
part 2/a
}

lonely 1/a

part 3/z`
    var goal = new rExMMPMP(fileGoal)
    let rEx = new rExMMPMP(fileContents)
    rEx.findOverwriteAfterUntil("3/", "part 3/z", "", "")
    assert.deepStrictEqual(rEx.FILELINES, goal.FILELINES)
    done()
  })

  it("adds new inside first matching until if empty or weak after", (done) => {
    var fileGoal = `
lonely 1/a

$part1 {
part 1/a
part 2/a
part 3/z
}

$part2 {
part 1/a
part 2/a
}

lonely 1/a
`
    var goal = new rExMMPMP(fileGoal)
    let rEx = new rExMMPMP(fileContents)
    rEx.findOverwriteAfterUntil("3/", "part 3/z", "", "}")
    assert.deepStrictEqual(rEx.FILELINES, goal.FILELINES)
    done()
  })

  it("adds new first matching until if empty or weak after", (done) => {
    var fileGoal = `
lonely 1/a

$part1 {
part 1/a
part 2/a
part 3/z
}

$part2 {
part 1/a
part 2/a
}

lonely 1/a
`
    var goal = new rExMMPMP(fileGoal)
    let rEx = new rExMMPMP(fileContents)
    rEx.findOverwriteAfterUntil("3/", "part 3/z", "", "}")
    assert.deepStrictEqual(rEx.FILELINES, goal.FILELINES)
    done()
  })
})
