"use strict"
// const path = require("path");
const assert = require("yeoman-assert")
const rExMMPMP = require("../regexpressionsmakemepeemypants.js")

var ANTSH1_BEESH2 = `
$any = "thing";
$ants (
 red,
)
$bees (
 honey,
)`
var BEES = `
$any = "thing";
$ants (

)
$bees (
 honey,
 red,
)`
var ANTS = `
$any = "thing";
$ants (
 red,
 honey,
)
$bees (

)`
var NEWANT = `
$any = "thing";
$ants (
 red,
 army,
)
$bees (
 honey,
)`
var SWAP = `
$any = "thing";
$ants (
 honey,
)
$bees (
 red,
)`

describe("tagname positioning in settings.scss", () => {
  it("confirms the .FILELINES matches the lines we provided", (done) => {
    let rEx = new rExMMPMP(ANTSH1_BEESH2)
    let fileLinesGoal = [
      "",
      '$any = "thing";',
      "$ants (",
      " red,",
      ")",
      "$bees (",
      " honey,",
      ")",
    ]
    assert.deepStrictEqual(rEx.FILELINES, fileLinesGoal)
    done()
  })
  it("confirms the .TESTSTATE matches the lines we provided", (done) => {
    let rEx = new rExMMPMP(ANTSH1_BEESH2)
    let fileLinesGoal = '$any = "thing";$ants ( red,)$bees ( honey,)'
    assert.deepStrictEqual(rEx.TESTSTATE, fileLinesGoal)
    done()
  })

  it("confirms the .FILEBACK property matches the file we initialized it with", (done) => {
    let rEx = new rExMMPMP(ANTSH1_BEESH2)
    assert.deepStrictEqual(rEx.FILEBACK, ANTSH1_BEESH2)
    done()
  })

  it("$ants red $bees honey ==> $ants $bees honey red", (done) => {
    let rEx = new rExMMPMP(ANTSH1_BEESH2)
    rEx.findOverwriteAfterUntil("red,", " red,", "$bees (", ")")
    var goal = new rExMMPMP(BEES)
    assert.deepStrictEqual(rEx.TESTSTATE, goal.TESTSTATE)
    done()
  })

  it("$ants red $bees honey ==> $ants honey red $bees", (done) => {
    let rEx = new rExMMPMP(ANTSH1_BEESH2)
    rEx.findOverwriteAfterUntil("honey,", " honey,", "$ants (", ")")
    var goal = new rExMMPMP(ANTS)
    assert.deepStrictEqual(rEx.TESTSTATE, goal.TESTSTATE)
    done()
  })

  it("$ants red $bees honey ==> $ants red army $bees honey", (done) => {
    let rEx = new rExMMPMP(ANTSH1_BEESH2)
    rEx.findOverwriteAfterUntil("army,", " army,", "$ants (", ")")
    var goal = new rExMMPMP(NEWANT)
    assert.deepStrictEqual(rEx.TESTSTATE, goal.TESTSTATE)
    done()
  })

  it("$ants red $bees honey ==> $ants honey $bees red ", (done) => {
    let rEx = new rExMMPMP(ANTSH1_BEESH2)
    rEx.findOverwriteAfterUntil("red,", " red,", "$bees (", ")")
    rEx.findOverwriteAfterUntil("honey,", " honey,", "$ants (", ")")
    var goal = new rExMMPMP(SWAP)
    assert.deepStrictEqual(rEx.TESTSTATE, goal.TESTSTATE)
    done()
  })

  it("$ants red $bees honey ==> $ants red $bees honey", (done) => {
    let rEx = new rExMMPMP(ANTSH1_BEESH2)
    // already there
    rEx.findOverwriteAfterUntil("red,", " red,", "$ants (", ")")
    var goal = new rExMMPMP(ANTSH1_BEESH2)
    assert.deepStrictEqual(rEx.TESTSTATE, goal.TESTSTATE)
    done()
  })
})
