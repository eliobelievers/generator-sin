"use strict"
// const path = require("path");
const assert = require("yeoman-assert")
const rExMMPMP = require("../regexpressionsmakemepeemypants.js")

var SWAPPED = `
$any = "thing";
$ants (
 army,
 bumble,
)
$bees (
 honey,
 red,
)`
var CRAMMED = `
$any = "thing";
$ants (
 red,
 army,
 bumble,
 honey,
)
$bees (
)`

describe("tagname positioning in settings.scss tags on same line", () => {
  var SAMELINE = `
$any = "thing";
$ants (
 red, army,
)
$bees (
 bumble, honey,
)`
  it("SWAPS $ants red, army, $bees bumble, honey, ==> $ants army, bumble, $bees honey, red,", (done) => {
    let rEx = new rExMMPMP(SAMELINE)
    rEx.findOverwriteAfterUntil("red,", " red,", "$bees (", ")")
    rEx.findOverwriteAfterUntil("bumble,", " bumble,", "$ants (", ")")
    var goal = new rExMMPMP(SWAPPED)
    assert.deepStrictEqual(rEx.TESTSTATE, goal.TESTSTATE)
    done()
  })

  it("CRAMS $ants red, army, $bees bumble, honey, ==> $ants red, army, bumble, honey,", (done) => {
    let rEx = new rExMMPMP(SAMELINE)
    rEx.findOverwriteAfterUntil("bumble,", " bumble,", "$ants (", ")")
    rEx.findOverwriteAfterUntil("honey,", " honey,", "$ants (", ")")
    var goal = new rExMMPMP(CRAMMED)
    assert.deepStrictEqual(rEx.TESTSTATE, goal.TESTSTATE)
    done()
  })
})

describe("tagname positioning in settings.scss tags on different lines", () => {
  var NEWLINES = `
$any = "thing";
$ants (
 red,
 army,
)
$bees (
 bumble,
 honey,
)`
  it("SWAPS $ants red, army, $bees bumble, honey, ==> $ants army, bumble, $bees honey, red,", (done) => {
    let rEx = new rExMMPMP(NEWLINES)
    rEx.findOverwriteAfterUntil("red,", " red,", "$bees (", ")")
    rEx.findOverwriteAfterUntil("bumble,", " bumble,", "$ants (", ")")
    var goal = new rExMMPMP(SWAPPED)
    assert.deepStrictEqual(rEx.TESTSTATE, goal.TESTSTATE)
    done()
  })

  it("CRAMS $ants red, army, $bees bumble, honey, ==> $ants red, army, bumble, honey, $bees ", (done) => {
    let rEx = new rExMMPMP(NEWLINES)
    rEx.findOverwriteAfterUntil("bumble,", " bumble,", "$ants (", ")")
    rEx.findOverwriteAfterUntil("honey,", " honey,", "$ants (", ")")
    var goal = new rExMMPMP(CRAMMED)
    assert.deepStrictEqual(rEx.TESTSTATE, goal.TESTSTATE)
    done()
  })
})
