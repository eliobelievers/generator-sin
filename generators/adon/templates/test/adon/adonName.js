const suites = require("../suites/adonTesterSuite");

var testML = "<h1>PRETEST</h1>";
var testQy = "h1";

suites.AdonTesterSuite(
  "jQuery.fn.extend.adon<%= adonName %>",
  "../../js/adon/adon<%= adonName %>.js",
  testML,
  function() {
    it("adon<%= adonName %> adds class to tag", done => {
      $(testQy)[0].should.not.have.class("adon<%= adonName %>");
      $(testQy).adon<%= adonName %>();
      $(testQy)[0].should.have.class("adon<%= adonName %>"); // true class
      done();
    });

    it("adof<%= adonName %> removes class from tag", done => {
      $(testQy).adon<%= adonName %>();
      $(testQy)[0].should.have.class("adon<%= adonName %>");
      $(testQy).adof<%= adonName %>();
      $(testQy)[0].should.not.have.class("adon<%= adonName %>"); // no class
      done();
    });
  }
);
