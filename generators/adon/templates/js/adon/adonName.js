// NB: Twinned with /stylesheets/adon/adon<%= adonName %>.css
// NB: Tested by /test/adon/adon<%= adonName %>.js
// Event handler:
// jQuery.fn.extend({
//   adonFullName: function() {
//     $(this).click(function() {
//        $(this).toggleClass("adonFullName")
//     })
//   })
// });
jQuery.fn.extend({
  adon<%= adonName %>: function() {
    return $(this).each(function() {
      $(this).addClass("adon<%= adonName %>");
      return $(this);
    });
  },
  _adof<%= adonName %>: function() {
    return $(this).each(function() {
      $(this).removeClass("adon<%= adonName %>");
      return $(this);
    });
  }
});
