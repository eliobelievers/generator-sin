"use strict"
const Thingrator = require("generator-thing/generators/thingrator")
const PutProngIn = require("../app/putprongin.js")
const __PRONG__ = new PutProngIn()

module.exports = class Adonappleeater extends Thingrator {
  initializing() {
    if (!this.options.composedBy) {
      this._eliosay("Generate an Adon")
      this._blackquote(
        `O much deceived, much failing, hapless Eve, \n` +
          `Of thy presumed return! \n`,
      )
    }
  }
  end() {
    if (!this.options.composedBy) {
      this._blackquote(
        `                     event perverse! \n` +
          `Thou never from that hour in Paradise` +
          `Foundst either sweet repast, or sound repose;`,
      )
    }
  }
  prompting() {
    const prompts = [
      {
        type: "input",
        name: "adon",
        message: "The name of the adon, e.g. FilterLikeMe",
        default: "NewAdon" || this.config.get("adon"),
      },
    ]
    return this.prompt(prompts).then((answer) => {
      this.adon = answer.adon
    })
  }
  writing() {
    this.paths()
    this._copy("test/suites/adonTesterSuite.js")
    this._adon("js/adon", this.adon, "js")
    this._adon("stylesheets/adon", this.adon, "scss")
    this._adon("test/adon", this.adon, "js")
    this.fs.write(
      this.destinationPath("stylesheets/theme.scss"),
      __PRONG__.theme(
        "adon",
        `adon${this.adon}`,
        this._getOrTemplate("stylesheets/theme.scss"),
      ),
    )
  }
  _adon(path, adonName, ext) {
    this._write(path + `/adonName.${ext}`, path + `/adon${adonName}.${ext}`, {
      adonName: adonName,
    })
  }
}
