const prongs = ["pillar", "hell", "heaven"]

String.prototype.prongListCleanUp = function () {
  return this.toLowerCase()
    .replace(/[^a-zA-Z0-9,()]+/g, "")
    .replace(",)", ")")
}

String.prototype.importListCleanUp = function () {
  return this.toLowerCase()
    .replace(/[s{2,}]/g, " ")
    .replace(";}", "}")
}

/* PutProngIn
 * Managing the settings and theme.scss files when adding or moving tagNames.
 */
module.exports = class PutProngIn {
  /** settings function
   * Add or move a tagName in `settings.scss`
   * @param {tagName} STR Name of the tag you are adding/moving.
   * @param {prongName} STR Name of the prong you are adding/moving the tag to.
   * @param {settingsString} STR String of your settings.scss file.
   * @returns `settingsString` with the theme in the correct prong.
   * @example See test file `__tests__/prong_set.js`
   */
  settings(tagName, prongName, settingsString) {
    // Move out of any prongs
    prongs.forEach((prong) => {
      let reg = "(?<=\\$" + prong + ":)([\\s\\S][^)]*)\\)"
      let find = new RegExp(reg, "g")
      let pronged = settingsString.match(find).toString()
      let depronged = pronged.prongListCleanUp()
      depronged = depronged.replace(`(${tagName},`, "(")
      depronged = depronged.replace(`,${tagName},`, ",")
      depronged = depronged.replace(`,${tagName})`, ")")
      depronged = depronged.replace(`(${tagName})`, "()")
      settingsString = settingsString.replace(pronged, " " + depronged)
    })
    // Add to the end of the prong's list
    let reg = "(?<=\\$" + prongName + ":)([\\s\\S][^)]*)\\)"
    let find = new RegExp(reg, "g")
    let pronged = settingsString.match(find).toString()
    let repronged = pronged.toString().prongListCleanUp()
    repronged = repronged.replace(`)`, `,${tagName})`)
    settingsString = settingsString.replace(pronged, " " + repronged)
    return settingsString
  }

  /** theme function
   * Add or move a tagName in `theme.scss`
   * @param {tagName} STR Name of the tag you are adding/moving.
   * @param {prongName} STR Name of the prong you are adding/moving the tag to.
   * @param {themeString} STR String of your theme.scss file.
   * @returns `themeString` with the theme in the correct prong.
   * @example See test file `__tests__/prong_theme.js`
   */
  theme(tagName, prongName, themeString) {
    // Get the list of `@import` statements from the container.
    let reg = "(?<=#\\{\\$container\\})([\\s\\S][^}]*)\\}"
    let find = new RegExp(reg, "g")
    let container = themeString.match(find)
    if (container) {
      let importStatements = container.toString()
      importStatements = importStatements.replace("{", "")
      importStatements = importStatements.replace("}", "")
      importStatements = importStatements.replace(".", "")
      importStatements = importStatements.split(";")
      // trim all statements
      importStatements = importStatements.map((importStatement) =>
        importStatement.trim(),
      )
      // remove empty
      importStatements = importStatements.filter((importStatement) =>
        importStatement.trim(),
      )
      // remove when matching target tagName
      importStatements = importStatements.filter((importStatement) => {
        return !importStatement.includes(`@import '${tagName}/`)
      })
      // Add to the end of the list
      importStatements.push(`@import '${tagName}/${prongName}'`)
      themeString = themeString.replace(
        container,
        " {\n  " + importStatements.join(";\n  ") + ";\n}",
      )
    }
    return themeString
  }
}
