"use strict"
const Thingrator = require("generator-thing/generators/thingrator")

// NB: This can't be tested.  It calls default.
module.exports = class Appitator extends Thingrator {
  initializing() {
    if (!this.options.composedBy) {
      this._eliosay("Generating Sin")
      this._blackquote(
        `                        To thee I have transferred \n` +
          ` All judgement, whether in Heaven, or [Pillar], or Hell.`,
      )
    }
    this.composeWith("sin:god", {
      arguments: [this.identifier, this.subjectOf],
      composedBy: Appitator, // god wont speak.
    })
  }
  end() {
    if (!this.options.composedBy) {
      this._blackquote(
        `Easy it may be seen that I intend \n` + `Mercy colleague with justice`,
      )
    }
  }
}
