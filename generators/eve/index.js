"use strict"
const Thingrator = require("generator-thing/generators/thingrator")
const PutProngIn = require("../app/putprongin.js")
const __PRONG__ = new PutProngIn()

module.exports = class Eveappleeater extends Thingrator {
  initializing() {
    if (!this.options.composedBy) {
      this._eliosay("Generating Eve")
      this._blackquote(
        `                          Whence hail to thee, \n` +
          `Eve rightly called, mother of all mankind, \n`,
        +`Mother of all things living,`,
      )
    }
  }
  end() {
    if (!this.options.composedBy) {
      this._blackquote(
        `                             since by thee \n` +
          `Man is to live; and all things live for Man.`,
      )
    }
  }
  prompting() {
    const prompts = [
      {
        type: "input",
        name: "sinner",
        message: "tagName of your sinner",
        default: "h1" || this.config.get("sinner"),
      },
      {
        type: "input",
        name: "position",
        message: "pillar/heaven/hell/atheist/theme",
        default: "pillar" || this.config.get("position"),
      },
    ]
    return this.prompt(prompts).then((answer) => {
      this.sinner = answer.sinner
      this.position = answer.position
      this.config.set("sinner", answer.sinner)
      this.config.set("position", answer.position)
    })
  }
  writing() {
    this.paths()

    // Write for the theme.
    this._write(
      "stylesheets/tagName/_" + this.position + ".scss",
      "stylesheets/" + this.sinner + "/_" + this.position + ".scss",
      {
        tagName: this.sinner,
      },
    )
    // Write a little settings files for the theme.
    this._write(
      "stylesheets/tagName/_settings.scss",
      "stylesheets/" + this.sinner + "/_settings.scss",
      {
        tagName: this.sinner,
      },
    )
    // Adjust the settings file.
    this.fs.write(
      this.destinationPath("stylesheets/settings.scss"),
      __PRONG__.settings(
        this.sinner,
        this.position,
        this._getOrTemplate("stylesheets/settings.scss"),
      ),
    )
    // Adjust the theme file.
    this.log(this._getOrTemplate("stylesheets/theme.scss"))
    this.fs.write(
      this.destinationPath("stylesheets/theme.scss"),
      __PRONG__.theme(
        this.sinner,
        this.position,
        this._getOrTemplate("stylesheets/theme.scss"),
      ),
    )
  }
}
