"use strict"
const path = require("path")
const assert = require("yeoman-assert")
const helpers = require("yeoman-test")

describe("generator-sin:god", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/god"))
      .inDir(path.join(__dirname, "test_generated", "god_test"))
      .withArguments(["noinstall"])
      .withOptions({ identifier: "hello" })
      .withPrompts({
        pillar: "h1",
        heaven: "h2",
        hell: "h3",
      })
      .on("error", function (error) {
        console.log("Oh Noes!", error)
      })
      .on("end", done)
  })

  it("no thing:repo files (it's a mocked generator)", () => {
    assert.noFile(".gitignore")
  })

  it("creates sin:god files shared with thing:repo", () => {
    assert.file("README.md")
    assert.file("package.json")
  })

  it("creates sin:god files", () => {
    assert.file("gulpfile.js")
    assert.file("index.html")

    assert.file("stylesheets/_pillar.scss")
    assert.file("stylesheets/_heaven.scss")
    assert.file("stylesheets/_hell.scss")
    assert.file("stylesheets/judge.scss")
    assert.file("stylesheets/settings.scss")
    assert.file("stylesheets/theme.scss")
    assert.file("stylesheets/functions/_i_am.scss")
    assert.file("stylesheets/mixins/_i_am.scss")
    assert.file("test/functions/_i_am.scss")
    assert.file("test/mixins/_i_am.scss")
    assert.file("test/suites/adonTesterSuite.js")
    assert.file("test/stylesheets.js")
    assert.file("test/stylesheets.scss")

    // styles for tagnames in their respective positions
    assert.file("stylesheets/h1/_pillar.scss")
    assert.file("stylesheets/h2/_heaven.scss")
    assert.file("stylesheets/h3/_hell.scss")

    assert.file("css/main.css")
    assert.file("css/normalize.css")
    assert.file("js/main.js")
    assert.file("js/plugins.js")
  })

  it("the templates are sound", () => {
    // settings file is sound
    assert.fileContent("stylesheets/settings.scss", "$pillar: (h1,);")
    assert.fileContent("stylesheets/settings.scss", "$heaven: (h2,);")
    assert.fileContent("stylesheets/settings.scss", "$hell: (h3,);")

    // test file is sound
    assert.fileContent(
      "test/stylesheets.scss",
      "@import './node_modules/@elioway/god/test/functions/thereisagod';",
    )
    assert.fileContent(
      "test/stylesheets.scss",
      "@import './node_modules/@elioway/eve/test/functions/godcreatedeve';",
    )
    assert.fileContent(
      "test/stylesheets.scss",
      "@import './node_modules/@elioway/adon/test/functions/godcreatedadon';",
    )
    assert.fileContent(
      "test/stylesheets.scss",
      "@import './node_modules/@elioway/innocent/test/functions/innocent';",
    )

    // theme file is sound
    assert.fileContent("stylesheets/theme.scss", "@import 'h1/pillar';")
    assert.fileContent("stylesheets/theme.scss", "@import 'h2/heaven';")
    assert.fileContent("stylesheets/theme.scss", "@import 'h3/hell';")
  })
})
