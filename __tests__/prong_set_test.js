"use strict"
// const path = require("path");
const assert = require("yeoman-assert")
const PutProngIn = require("../generators/app/putprongin.js")
let __PRONG__ = new PutProngIn()

describe("PutProngIn.settings(tagName, prongName, settingsString) to edit the settings file and position a tagName exclusively into a prong", () => {
  const settingsString = `$container: (body, main); // container
$pillar: (
  h1,
  h2,
  p,
); // pillar
$heaven:     (
  blockquote,
        h3
   );
$hell: (aside);`

  it("moves first tag from pillar to heaven", (done) => {
    let repronged = __PRONG__.settings("h1", "heaven", settingsString)
    var confirm = `$container: (body, main); // container
$pillar: (h2,p); // pillar
$heaven: (blockquote,h3,h1);
$hell: (aside);`
    assert.deepEqual(repronged, confirm)
    done()
  })

  it("moves last tag from pillar to heaven", (done) => {
    let repronged = __PRONG__.settings("p", "heaven", settingsString)
    var confirm = `$container: (body, main); // container
$pillar: (h1,h2); // pillar
$heaven: (blockquote,h3,p);
$hell: (aside);`
    assert.deepEqual(repronged, confirm)
    done()
  })

  it("moves tags from pillar to hell", (done) => {
    let repronged = __PRONG__.settings("h1", "hell", settingsString)
    var confirm = `$container: (body, main); // container
$pillar: (h2,p); // pillar
$heaven: (blockquote,h3);
$hell: (aside,h1);`
    assert.deepEqual(repronged, confirm)
    done()
  })

  it("moves the first heaven tag to pillar", (done) => {
    let repronged = __PRONG__.settings("blockquote", "pillar", settingsString)
    var confirm = `$container: (body, main); // container
$pillar: (h1,h2,p,blockquote); // pillar
$heaven: (h3);
$hell: (aside);`
    assert.deepEqual(repronged, confirm)
    done()
  })

  it("moves the last heaven tag to hell", (done) => {
    let repronged = __PRONG__.settings("h3", "hell", settingsString)
    var confirm = `$container: (body, main); // container
$pillar: (h1,h2,p); // pillar
$heaven: (blockquote);
$hell: (aside,h3);`
    assert.deepEqual(repronged, confirm)
    done()
  })

  it("moves the first hell tag to pillar", (done) => {
    let repronged = __PRONG__.settings("aside", "pillar", settingsString)
    var confirm = `$container: (body, main); // container
$pillar: (h1,h2,p,aside); // pillar
$heaven: (blockquote,h3);
$hell: ();`
    assert.deepEqual(repronged, confirm)
    done()
  })

  it("moves the last hell tag to heaven", (done) => {
    let repronged = __PRONG__.settings("aside", "heaven", settingsString)
    var confirm = `$container: (body, main); // container
$pillar: (h1,h2,p); // pillar
$heaven: (blockquote,h3,aside);
$hell: ();`
    assert.deepEqual(repronged, confirm)
    done()
  })

  it("it moves the tags to the end of its current prongs bracketed lists, if there is no change", (done) => {
    let repronged = __PRONG__.settings("h1", "pillar", settingsString)
    var confirm = `$container: (body, main); // container
$pillar: (h2,p,h1); // pillar
$heaven: (blockquote,h3);
$hell: (aside);`
    assert.deepEqual(repronged, confirm)
    done()
  })

  it("it add tags to the end of its current prongs", (done) => {
    let repronged = __PRONG__.settings("h6", "pillar", settingsString)
    var confirm = `$container: (body, main); // container
$pillar: (h1,h2,p,h6); // pillar
$heaven: (blockquote,h3);
$hell: (aside);`
    assert.deepEqual(repronged, confirm)
    done()
  })
})
