"use strict"
// const path = require("path");
const assert = require("yeoman-assert")
const PutProngIn = require("../generators/app/putprongin.js")
let __PRONG__ = new PutProngIn()

describe("PutProngIn.theme(tagName, prongName, themeString) to edit the theme file and position a tagName exclusively into a prong", () => {
  const themeString = `@import 'some/mixins' // required;
#{$container} {
  @import 'pillar';
  @import 'p/pillar';
  @import 'h2/heaven';
  @import 'h3/hell';
}`

  it("leaves tag from pillar to pillar", (done) => {
    let repronged = __PRONG__.theme("p", "pillar", themeString)
    var confirm = `@import 'some/mixins' // required;
#{$container} {
  @import 'pillar';
  @import 'h2/heaven';
  @import 'h3/hell';
  @import 'p/pillar';
}`
    assert.deepEqual(repronged, confirm)
    done()
  })

  it("moves tag from hell to pillar", (done) => {
    let repronged = __PRONG__.theme("h3", "pillar", themeString)
    var confirm = `@import 'some/mixins' // required;
#{$container} {
  @import 'pillar';
  @import 'p/pillar';
  @import 'h2/heaven';
  @import 'h3/pillar';
}`
    assert.deepEqual(repronged, confirm)
    done()
  })

  it("moves tag from heaven to pillar", (done) => {
    let repronged = __PRONG__.theme("h2", "pillar", themeString)
    var confirm = `@import 'some/mixins' // required;
#{$container} {
  @import 'pillar';
  @import 'p/pillar';
  @import 'h3/hell';
  @import 'h2/pillar';
}`
    assert.deepEqual(repronged, confirm)
    done()
  })

  it("adds tag to pillar", (done) => {
    let repronged = __PRONG__.theme("h4", "pillar", themeString)
    var confirm = `@import 'some/mixins' // required;
#{$container} {
  @import 'pillar';
  @import 'p/pillar';
  @import 'h2/heaven';
  @import 'h3/hell';
  @import 'h4/pillar';
}`
    assert.deepEqual(repronged, confirm)
    done()
  })

  it("moves tag from pillar to heaven", (done) => {
    let repronged = __PRONG__.theme("p", "heaven", themeString)
    var confirm = `@import 'some/mixins' // required;
#{$container} {
  @import 'pillar';
  @import 'h2/heaven';
  @import 'h3/hell';
  @import 'p/heaven';
}`
    assert.deepEqual(repronged, confirm)
    done()
  })

  it("moves tag from hell to heaven", (done) => {
    let repronged = __PRONG__.theme("h3", "heaven", themeString)
    var confirm = `@import 'some/mixins' // required;
#{$container} {
  @import 'pillar';
  @import 'p/pillar';
  @import 'h2/heaven';
  @import 'h3/heaven';
}`
    assert.deepEqual(repronged, confirm)
    done()
  })

  it("leaves tag from heaven to heaven", (done) => {
    let repronged = __PRONG__.theme("h2", "heaven", themeString)
    var confirm = `@import 'some/mixins' // required;
#{$container} {
  @import 'pillar';
  @import 'p/pillar';
  @import 'h3/hell';
  @import 'h2/heaven';
}`
    assert.deepEqual(repronged, confirm)
    done()
  })

  it("adds tag to heaven", (done) => {
    let repronged = __PRONG__.theme("h4", "heaven", themeString)
    var confirm = `@import 'some/mixins' // required;
#{$container} {
  @import 'pillar';
  @import 'p/pillar';
  @import 'h2/heaven';
  @import 'h3/hell';
  @import 'h4/heaven';
}`
    assert.deepEqual(repronged, confirm)
    done()
  })

  it("moves tag from pillar to hell", (done) => {
    let repronged = __PRONG__.theme("p", "hell", themeString)
    var confirm = `@import 'some/mixins' // required;
#{$container} {
  @import 'pillar';
  @import 'h2/heaven';
  @import 'h3/hell';
  @import 'p/hell';
}`
    assert.deepEqual(repronged, confirm)
    done()
  })

  it("leaves tag from hell to hell", (done) => {
    let repronged = __PRONG__.theme("h3", "hell", themeString)
    var confirm = `@import 'some/mixins' // required;
#{$container} {
  @import 'pillar';
  @import 'p/pillar';
  @import 'h2/heaven';
  @import 'h3/hell';
}`
    assert.deepEqual(repronged, confirm)
    done()
  })

  it("moves tag from heaven to hell", (done) => {
    let repronged = __PRONG__.theme("h2", "hell", themeString)
    var confirm = `@import 'some/mixins' // required;
#{$container} {
  @import 'pillar';
  @import 'p/pillar';
  @import 'h3/hell';
  @import 'h2/hell';
}`
    assert.deepEqual(repronged, confirm)
    done()
  })

  it("adds tag to hell", (done) => {
    let repronged = __PRONG__.theme("h4", "hell", themeString)
    var confirm = `@import 'some/mixins' // required;
#{$container} {
  @import 'pillar';
  @import 'p/pillar';
  @import 'h2/heaven';
  @import 'h3/hell';
  @import 'h4/hell';
}`
    assert.deepEqual(repronged, confirm)
    done()
  })
})
