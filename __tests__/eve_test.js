"use strict"
const path = require("path")
const assert = require("yeoman-assert")
const helpers = require("yeoman-test")

// var deps = [
//   [helpers.createDummyGenerator(), "sin:god"],
//   [helpers.createDummyGenerator(), "thing:repo"],
// ]

describe("generator-sin:eve pillar", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/eve"))
      .inDir(path.join(__dirname, "test_generated", "eve_pillar"))
      .withPrompts({
        sinner: "h1",
        position: "pillar",
      })
      .on("error", function (error) {
        console.log("Oh Noes!", error)
      })
      .on("end", done)
  })

  it("creates files", (done) => {
    assert.file("stylesheets/h1/_pillar.scss")
    assert.fileContent("stylesheets/h1/_pillar.scss", "h1 {")
    assert.noFile("stylesheets/h1/_atheist.scss")
    assert.noFile("stylesheets/h1/_hell.scss")
    assert.noFile("stylesheets/h1/_heaven.scss")
    assert.noFile("stylesheets/h1/_theme.scss")
    done()
  })

  it("puts the tagname into the settings.scss file - right position and everything", (done) => {
    assert.file("stylesheets/settings.scss")
    assert.fileContent("stylesheets/settings.scss", "$pillar: (p,h1)")
    done()
  })

  it("puts the tagname into the theme.scss file - right position and everything", (done) => {
    assert.file("stylesheets/theme.scss")
    assert.fileContent("stylesheets/theme.scss", "@import 'h1/pillar';")
    done()
  })
})

describe("generator-sin:eve hell", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/eve"))
      .inDir(path.join(__dirname, "test_generated", "eve_hell"))
      .withPrompts({
        sinner: "h1",
        position: "hell",
      })
      .on("error", function (error) {
        console.log("Oh Noes!", error)
      })
      .on("end", done)
  })

  it("creates files", (done) => {
    assert.file("stylesheets/h1/_hell.scss")
    assert.fileContent("stylesheets/h1/_hell.scss", "h1 {")
    assert.noFile("stylesheets/h1/_atheist.scss")
    assert.noFile("stylesheets/h1/_heaven.scss")
    assert.noFile("stylesheets/h1/_pillar.scss")
    assert.noFile("stylesheets/h1/_theme.scss")
    done()
  })

  it("puts the tagname into the settings.scss file - right position and everything", (done) => {
    assert.file("stylesheets/settings.scss")
    assert.fileContent("stylesheets/settings.scss", "$hell: (aside,h1);")
    done()
  })

  it("puts the tagname into the theme.scss file - right position and everything", (done) => {
    assert.file("stylesheets/theme.scss")
    assert.fileContent("stylesheets/theme.scss", "@import 'h1/hell';")
    done()
  })
})

describe("generator-sin:eve heaven", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/eve"))
      .inDir(path.join(__dirname, "test_generated", "eve_heaven"))
      .withPrompts({
        sinner: "h1",
        position: "heaven",
      })
      .on("error", function (error) {
        console.log("Oh Noes!", error)
      })
      .on("end", done)
  })

  it("creates files", (done) => {
    assert.file("stylesheets/h1/_heaven.scss")
    assert.fileContent("stylesheets/h1/_heaven.scss", "h1 {")
    assert.noFile("stylesheets/h1/_atheist.scss")
    assert.noFile("stylesheets/h1/_hell.scss")
    assert.noFile("stylesheets/h1/_pillar.scss")
    assert.noFile("stylesheets/h1/_theme.scss")
    done()
  })

  it("puts the tagname into the settings.scss file - right position and everything", (done) => {
    assert.file("stylesheets/settings.scss")
    assert.fileContent("stylesheets/settings.scss", "$heaven: (blockquote,h1);")
    done()
  })

  it("puts the tagname into the theme.scss file - right position and everything", (done) => {
    assert.file("stylesheets/theme.scss")
    assert.fileContent("stylesheets/theme.scss", "@import 'h1/heaven';")
    done()
  })
})

describe("generator-sin:eve move pillar to heaven settings rewrite", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/eve"))
      .inDir(path.join(__dirname, "test_generated", "eve_move_pillar_heaven"))
      .withPrompts({
        sinner: "p",
        position: "heaven",
      })
      .on("error", function (error) {
        console.log("Oh Noes!", error)
      })
      .on("end", done)
  })

  it("creates files", (done) => {
    assert.file("stylesheets/p/_heaven.scss")
    assert.fileContent("stylesheets/p/_heaven.scss", "p {")
    done()
  })

  it("moves the tagname in the settings.scss file - right position and everything", (done) => {
    assert.file("stylesheets/settings.scss")
    assert.fileContent("stylesheets/settings.scss", "$pillar: ();")
    assert.fileContent("stylesheets/settings.scss", "$heaven: (blockquote,p);")
    assert.fileContent("stylesheets/settings.scss", "$hell: (aside);")
    done()
  })

  it("puts the tagname into the theme.scss file - right position and everything", (done) => {
    assert.file("stylesheets/theme.scss")
    assert.fileContent("stylesheets/theme.scss", "@import 'p/heaven';")
    assert.noFileContent("stylesheets/theme.scss", "@import 'p/pillar';")
    done()
  })
})
