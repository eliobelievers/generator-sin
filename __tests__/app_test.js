"use strict"
const path = require("path")
const assert = require("yeoman-assert")
const helpers = require("yeoman-test")

var deps = [[helpers.createDummyGenerator(), "sin:god"]]

describe("generator-sin:app", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/app"))
      .inDir(path.join(__dirname, "test_generated", "app_test"))
      .withArguments(["noinstall"])
      .withGenerators(deps)
      .withOptions({ identifier: "hello" })
      .withPrompts({
        pillar: "h1",
        heaven: "h2",
        hell: "h3",
      })
      .on("error", function (error) {
        console.log("Oh Noes!", error)
      })
      .on("end", done)
  })

  it("creates nothing without god. not even a README!", () => {
    assert.noFile("README.md")
    assert.noFile("package.json")
  })
})
