"use strict"
const path = require("path")
const assert = require("yeoman-assert")
const helpers = require("yeoman-test")

describe("generator-sin:adon", () => {
  beforeAll((done) => {
    helpers
      .run(path.join(__dirname, "../generators/adon"))
      .inDir(path.join(__dirname, "test_generated", "adon"))
      .withPrompts({
        adon: "TestMeBaby",
      })
      .on("error", function (error) {
        console.log("Oh Noes!", error)
      })
      .on("end", done)
  })

  it("creates files", (done) => {
    assert.file("js/adon/adonTestMeBaby.js")
    assert.file("stylesheets/adon/adonTestMeBaby.scss")
    assert.file("test/adon/adonTestMeBaby.js")
    assert.file("test/suites/adonTesterSuite.js")
    done()
  })

  it("puts adonTestMeBaby into the theme.scss file - right position and everything", (done) => {
    assert.file("stylesheets/theme.scss")
    assert.fileContent(
      "stylesheets/theme.scss",
      `@import 'adon/adonTestMeBaby';`,
    )
    done()
  })

  it("js/adon/adonTestMeBaby.js template sound", (done) => {
    assert.fileContent(
      "js/adon/adonTestMeBaby.js",
      `adonTestMeBaby: function() {`,
    )
    assert.fileContent(
      "js/adon/adonTestMeBaby.js",
      `_adofTestMeBaby: function() {`,
    )
    assert.fileContent(
      "js/adon/adonTestMeBaby.js",
      `$(this).addClass("adonTestMeBaby")`,
    )
    assert.fileContent(
      "js/adon/adonTestMeBaby.js",
      `$(this).addClass("adonTestMeBaby")`,
    )
    done()
  })

  it("stylesheets/adon/adonTestMeBaby.scss template sound", (done) => {
    assert.fileContent(
      "stylesheets/adon/adonTestMeBaby.scss",
      `.adonTestMeBaby {`,
    )
    done()
  })

  it("test/adon/adonTestMeBaby.js template sound", (done) => {
    assert.fileContent(
      "test/adon/adonTestMeBaby.js",
      `jQuery.fn.extend.adonTestMeBaby`,
    )
    assert.fileContent(
      "test/adon/adonTestMeBaby.js",
      `../../js/adon/adonTestMeBaby.js`,
    )
    assert.fileContent(
      "test/adon/adonTestMeBaby.js",
      `$(testQy)[0].should.not.have.class("adonTestMeBaby");`,
    )
    done()
  })
})
