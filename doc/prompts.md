# generator-sin Prompts

## Your sinful identifier

Convention would be to use the same identifier as your folder: i.e. `genesis` in this example.

## Tags going into the pillar

These would be the tagNames you'll be using for your main page content which sits as a column in the centre of the page. For instance, you could just choose `div` here (and nest the rest of your html), but **the elioWay** would be to choose primary block tagNames like `h1`, `p`, `ul`, `ol`, `table`, `form`, `article`, etc.

## Tags bound for heaven

Content in these tags will be floated into the right gutter called _heaven_. The elioWay would be choose block or inlines tagNames whose content is likely to need moving out of the _pillarer_ content. Tags like `aside`, `h6`, `blockquote` `summary`, come to mind as suitable - but you can be creative. You could put `dl` in _heaven_ and use this tag to present glossary data alongside content in the _pillar_. Try to use tagNames for the reasons they were original intended.

## Tags sent to hell

Content in these tags will be floated into the left gutter, otherwise _hell_ tags are treated by **god** in a similar way to _heaven_ tags.

## Would you like HTML5 Boilerplate?

Say "Y" only if want HTML5 Boilerplate extra files added to your project. The H5BP generator comes with its own question for adding a docs folder. It's not important.
