# Contributing

.. is also a fine way to install **1.8.0** using GIT as a starting project.

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/eliosin.git
cd eliosin
git clone https://gitlab.com/eliosin/generator-sin.git
cd generator-sin
npm i|yarn
npm link generator-sin
gulp
```

## Testing in development

```shell
cd ~/repo/elioway/eliosin/generator-sin
npm|yarn link
cd ~/repo/newtheme
npm|yarn link generator-sin
yo sin
```

## Testing after development

```shell
cd ~/repo/elioway/eliosin/generator-sin
npm|yarn unlink
cd ~/repo/newtheme
npm i|yarn add generator-sin
```

## TODOS

1. Confirm in `yo sin`, but could apply to all generators. Reproduce:

```
cd root1
yo thing --identifier sin1
cd sin1

# no issue: adds root1/sin1/artwork
yo angels:artwork

# no issue: adds root1/sin1/stylesheets
yo sin:god

# issue: adds root1/sin1/sin1/stylesheets
yo sin

# issue: adds root1/sin1/sin1/stylesheets
yo sin --identifier sin1
# `god` is the default generator - this should work!

# issue: adds root1/sin1/sin1/stylesheets
yo sin --identifier sin1 --subjectOf root1
# `god` is the default generator - this should work!
```
