# generator-sin Filestructure

## What `yo sin` makes

```asciiart
|- eliosinApp
    |- index.html
    |- js
    |- stylesheets
    |- test
```

## `index.html` file

Use this to test your eliosin theme, or host your node app.

## `js` folder

These will contain files to support **adon** s.

```asciiart

    |- js
        |- main.js
        |- plugins.js
```

If you type `yo sin:`

## `stylesheets` folder

This is the folder where you'll do most work.

```asciiart

    |- stylesheets
        |- _heaven.scss
        |- _hell.scss
        |- _heaven.scss
        |- _pillar.scss
        |- judge.scss
        |- settings.scss
        |- theme.scss
        |- functions
            |- i_am.scss
        |- mixins
            |- _heaven.scss
            |- _hell.scss
        |- <tagName>
            |- <position>.scss
```

### stylesheets/pillar.scss

A CSS style for ALL the tags in your pillar.

### stylesheets/heaven.scss

A CSS style for ALL the tags in heaven.

### stylesheets/hell.scss

A CSS style for ALL the tags in hell.

### stylesheets/judge.scss

Called by the gulp file in genesis to build the genesis theme.

### stylesheets/settings.scss

The style settings for the genesis theme.

### stylesheets/theme.scss

To be imported into other eliosinners wanting to borrow/start from the genesis theme.

### stylesheets/_/_.scss

For each tagname mentioned in your answers, there is a `_pillar.scss`, `_heaven.scss`, `_hell.scss` and an `_athiest.scss` CSS style file. If you intend your theme to be reusable, **the elioWay** would be to create a version of the style for whereever the tag might be positioned by other users of it. Only the ones relevant to your chosen positions are mentioned in the `theme.scss` file. Else you can delete the ones you don't need.

### .png/ico/svg

eliosin themed artwork which replaces artwork added by HTML5 Boilerplate. You can swap these out.

## What the HTML5 Boilerplate generator makes

### css

Contains `main.css` + `normalize.css`.

### img

Put any images for your theme or web app in here.

### \*

Various files for deployment (see HTML5 Boilerplate doc).

## `test` folder

This is the folder where you can test any mixins or functions you create for your theme.

```asciiart
|- test
    |- stylesheets.js
    |- test.scss
    |- functions
        |- i_am.js
    |- mixins
        |- _heaven.scss
        |- _hell.scss
    |- suites
        |- adonTesterSuite.js
```
