# Prerequisites

- [eliosin Prerequisites](/eliosin/installing.html)

First, install [Yeoman](http://yeoman.io) and generator-tew using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).
