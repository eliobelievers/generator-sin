# Quickstart generator-sin

- [generator-sin Prerequisites](/eliosin/generator-sin/prerequisites.html)
- [Installing generator-sin](/eliosin/generator-sin/installing.html)

## Nutshell

- Create folder for your app.
- Enter the command `yo sin` in the folder.

### `yo sin`

```shell
mkdir genesis
cd genesis
yo sin
```

- Accept the defaults to every question.

```shell
? Tags going into the pillar h1, h2, h3, h4, p, ul, ol, dl, table,
? Tags bound for heaven blockquote, h6, summary,
? Tags sent to hell nav, aside, details,
? Would you like [HTML5 Boilerplate](LICENSE.txt)? y/N
```

- [See generator-sin Prompts](/eliosin/generator-sin/prompts)

- Now wait...

- Files are created.

- The generator runs the `yarn` command to install project's requirements.

- The finished message appears.

Type:

```shell
gulp
```

You're ready to go!
